variable "gcp_project" {
  description = "Google Cloud Platform project name"
  type        = string
}

variable "gitlab_token" {
  description = "GitLab access token"
  type        = string
}

variable "instance_name" {
  description = "Name of the Google Compute Engine instance"
  type        = string
}

variable "ssh_public_key" {
  description = "Path of the SSH Public Key"
  type = string
  default = "~/.ssh/id_rsa.pub"
  
}

variable "ssh_private_key" {
  description = "Path of the SSH Private Key"
  type = string
  default = "~/.ssh/id_rsa"
}
