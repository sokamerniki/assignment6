
resource "google_compute_instance" "vm" {
  name         = "sokamerniki-v4"
  machine_type = "n1-standard-1"
  zone         = "us-west1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }

  metadata_startup_script = "sudo useradd -m -p '' ansible && sudo usermod -aG sudo ansible"

  metadata = {
    ssh-keys = "ansible:${file("~/.ssh/id_rsa.pub")}"
  }

  provisioner "remote-exec" {
    inline = [
        "sudo apt-get update",
        "sudo apt-get install -y curl openssh-server ca-certificates",
        "curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash",
        "sudo EXTERNAL_URL=\"http://$(curl -s http://whatismyip.akamai.com)\" apt-get install gitlab-ce -y",
    ]

    connection {
        host        = google_compute_instance.vm.network_interface[0].access_config[0].nat_ip
        type        = "ssh"
        user        = "ansible" 
        private_key = file(var.ssh_private_key)
    }
    }
}

