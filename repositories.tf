# repositories.tf

provider "gitlab" {
  alias = "main"
}

resource "gitlab_project" "project1" {
  provider = gitlab
  name        = "project1"
  description = "My awesome codebase"

  visibility_level = "public"
}

resource "gitlab_project" "project2" {
  provider = gitlab
  name            = "project2"
  description = "My awesome codebase"

  visibility_level = "public"
}

resource "gitlab_project" "project3" {
  provider = gitlab
  name            = "project3"
  description = "My awesome codebase"

  visibility_level = "public"
}
